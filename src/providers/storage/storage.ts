import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Events } from 'ionic-angular';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Storage } from '@ionic/storage';

// import { Http } from '@angular/http';
import { Zip } from '@ionic-native/zip';

@Injectable()
export class StorageProvider {

	public version: number = 0;

	// Cart content
	public cart:any = {};
	public cartAddress:any = {};

	// Products
	public products:any = {};
	public feedbacks:any = [];
	public halls:any = {};
	public news:any = [];
	public categories:any = {};

	readonly urlSendOrder:string = "http://strazek.ru/application/send-order/";
	readonly urlSendTableOrder:string = "http://strazek.ru/application/send-table-order/";
	readonly urlSendFeedback:string = "http://strazek.ru/application/send-feedback/";
	readonly urlUpdate:string = "http://strazek.ru/application/update/";


	// Construct
  constructor(private zip:Zip, private httpClient: HttpClient, private events: Events, private transfer: FileTransfer, private file: File, private localStorage: Storage) {}

 	// Send order for table
 	sendTableOrder(inputData:any) {
		let storage = this;

    	let promise = new Promise((resolve, reject) => {
      		storage.httpClient.post(storage.urlSendTableOrder, inputData).subscribe( data => {
	        	resolve(true);
	     	}, error => {
	        	resolve(false);}
	    	);
    	});
    	return promise;
 	}

 	// Send order for menu products
 	sendOrder(inputData:any) {
 		let storage = this;

    	let promise = new Promise((resolve, reject) => {
      		storage.httpClient.post(storage.urlSendOrder, inputData).subscribe( data => {
	        	resolve(true);
	     	}, error => {
	     		resolve(false);}
	    	);
    	});
    	return promise;
 	}

 	// Send feedback to the site
 	// ------------------------------------------
 	sendFeedback(inputData:any) {  		
 		let storage = this;

    	let promise = new Promise((resolve, reject) => {
      		storage.httpClient.post(storage.urlSendFeedback, inputData).subscribe( data => {
				storage.events.publish("feedback:complete");				
	        	resolve(true);
	     	}, error => {
				storage.events.publish("feedback:fail");
	        	resolve(false);}
	    	);
    	});
    	return promise;
 	}

 	// Load data from JSON
 	// --------------------------------------------
 	loadJSON() {
 		let storage = this;
		storage.events.publish("update:init");

		// Read JSON content and parse it
		this.file.readAsText(this.file.dataDirectory, "info.json").then((content) => {

			let data:any = JSON.parse(content);

			if (data["categories"] != null) storage.categories = data["categories"];
			if (data["products"] != null) storage.products = data["products"];
			if (data["halls"] != null) storage.halls = data["halls"];
			if (data["news"] != null) storage.news = data["news"];
			if (data["feedbacks"] != null) storage.feedbacks = data["feedbacks"];

			storage.events.publish("update:finished");
		}, (error) => {
			console.error("Cant parse JSON", error.message);
		});
 	}

 	// Download the update file
	downloadUpdateFile(fileID:string) {

		let storage = this;
		
 		// Try to transfer the file
 		storage.events.publish("update:download");

		let url = "https://strazek.ru/application/files/" + fileID + ".zip";
 		let dir = storage.file.cacheDirectory;

		console.log("Require to update. Download URL:", url);
 		const fileTransfer: FileTransferObject = this.transfer.create();

 		// Check if path is exists
 		if (dir == null) {
 			console.log("Download directory is not found");
 			storage.events.publish("update:error");
 			return;
 		}

 		// ZIP file to download
 		let zipFileName = dir + "update.zip";
		console.log("Store to:", zipFileName);

 		// Store file to temp
 		fileTransfer.download(url, zipFileName, true).then((entry) => {

 			console.log("Downloading complete");
 	    let file = entry.nativeURL;

  		storage.events.publish("update:unpack");

      // Unpack file
 			this.zip.unzip(file, storage.file.dataDirectory, (progress) => {})
 			 .then((result) => {
 			   if (result === 0) {
			 			console.log("Unpack complete");
 			   		storage.events.publish("update:complete");
 			   		storage.localStorage.set("updateTime", fileID);
 			   		storage.loadJSON();
 			   }
 			   else {
 			   	console.log("Can not unzip file")
 			   	storage.events.publish("update:error");
 			   }
 			});
 	  }, (error) => {
			storage.events.publish("update:network-error");
 	    console.error("File download error", JSON.stringify(error), error.message);
 		});

	}

 	// Try to update
  update() {

    let storage:StorageProvider = this;

	  this.localStorage.get("updateTime").then((val) => {

	  	if (val == null) val = 0;
	  	let version = val;

			storage.events.publish("update:start");
			console.log("Start to check updates");

	  	// Send request update
      storage.httpClient.get(storage.urlUpdate).subscribe(data => {

				console.log("Got update status from the website");

				// Update is not required but we need to check if data files are exists
				if (data == null) {
	        console.error("Update status is not detected");
				}
				// Data was sent
				else if (data != null) {

					// We need to downlowd the update
					if (version < data["lastUpdateTime"]) {
						console.log("Last update file is old. Download a new one");
						storage.downloadUpdateFile(data["lastUpdateTime"]);
					}

					// Update is not required
					else if (data["code"] != null && data["code"] == "notRequired") {
	          console.log("Update is not required");

	          // If file exists
	          // ---------------
						storage.file.checkFile(storage.file.dataDirectory, "info.json").then((result) => {
							// If file is not exists, download it
							if (result != null && result == true) {
			          console.log("Data file is exists");
			          storage.events.publish("update:complete");
								storage.loadJSON();
							} else {
			          console.log("Data file is not exists, repeat to download it");
								storage.downloadUpdateFile(data["lastUpdateTime"]);
							}
						} , (error) => {
				 	    console.error("Cant check data file", error.message);
							storage.downloadUpdateFile(data["lastUpdateTime"]);
			 			});

					}
					
					// Data 
					else {
						console.log("Require to update");
						storage.downloadUpdateFile(data["lastUpdateTime"]);
					}
				}

  		},
  		// Connection errors
  		err => {
        console.error(err.message);
        storage.events.publish("update:connect-error");
      });

	  });
  }
}
