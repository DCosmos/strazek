import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicPageModule } from 'ionic-angular';
import { NewsPage } from './news';
import { MasonryModule } from 'angular2-masonry';

@NgModule({
  declarations: [
    NewsPage,
  ],
  imports: [
    IonicPageModule.forChild(NewsPage), MasonryModule
  ],
	schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  entryComponents: [ NewsPage ]
})
export class NewsPageModule {}
