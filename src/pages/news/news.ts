import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';
import { MasonryOptions } from 'angular2-masonry';

@IonicPage()
@Component({
  selector: 'page-news',
  templateUrl: 'news.html',
})


export class NewsPage {

	public newsList: any; // All displayable news are here

	// Masonry options
	public masonryOptions: MasonryOptions = { 
		transitionDuration: "0s",
  	fitWidth: true,
  	gutter: 5,
  	resize: true
	};

	// Init
  constructor(public navCtrl: NavController, public navParams: NavParams, storage: StorageProvider) {

 		// Get news from database
  	let news = storage.news;
  	let tempList = Object.keys(news).map(key=>news[key]);

  	// Sort by date
		let compare = function (a,b) {
		  if (a.publishDate < b.publishDate) return 1;
		  if (a.publishDate > b.publishDate) return -1;
		  return 0;
		}
		tempList.sort(compare);

		// Set list
  	this.newsList = tempList;
  }

  // Init masonry
  ionViewDidEnter() {
//		setTimeout(() => {
//			var elem = document.querySelector(".items-container");
//			var msnry = new Masonry(elem, {
//				itemSelector: "ion-card",
//				fitWidth: true
//			});
//		}, 100);
	}

}
