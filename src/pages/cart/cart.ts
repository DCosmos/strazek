import { Component, ViewChild } from '@angular/core';
import { Content, IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';

@IonicPage()
@Component({
	selector: 'page-cart',
	templateUrl: 'cart.html',
})

export class CartPage {

	@ViewChild(Content) content: Content;

	public items: any;

	constructor(private nav: NavController, private navParams: NavParams, private storage: StorageProvider, private events: Events) {
		let page:any = this;
		this.update();
		this.events.subscribe("cart:updated", () => { page.update();});
	}

	// Update elements list
	update() {

		// Init items
		this.items = [];

		// Skip empty cart
		if (Object.keys(this.storage.cart).length === 0) {
			if (this.nav.length() > 0) this.nav.pop();
			else this.nav.setPages([{"page" : "MenuPage"}]);
			return;
		}

		// Add items to list
		for (let item in this.storage.cart) {
			this.items.push({id: item});
		}

		if (this.content != null) this.content.resize();
	}

	// Open order form
	openOrderForm() {
		this.nav.pop();
		this.nav.push("OrderPage");
	}

}
