import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicPageModule } from 'ionic-angular';
import { CartPage } from './cart';
import { ComponentsModule} from '../../components/components.module';

@NgModule({
  declarations: [
    CartPage,
  ],
  imports: [
		ComponentsModule,
    IonicPageModule.forChild(CartPage),
  ],
	schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  entryComponents: [ CartPage ],  
})
export class CartPageModule {}