import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicPageModule } from 'ionic-angular';
import { ComponentsModule} from '../../components/components.module';
import { OrderPage } from './order';

@NgModule({
  declarations: [
    OrderPage
  ],
  imports: [
  	ComponentsModule,
    IonicPageModule.forChild(OrderPage)
  ],
	schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  entryComponents: [ OrderPage ]
})
export class OrderPageModule {}