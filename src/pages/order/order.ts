import { Component, ViewChild } from '@angular/core';
import { Content, NavController } from 'ionic-angular';
import { AlertController, Events } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';

@IonicPage()
@Component({ selector: 'page-order', templateUrl: 'order.html' })
export class OrderPage {

	@ViewChild(Content) content: Content;

	public time:string;
	public name:string;
	public phone: string
	public text:string;
	public hall:string;
	public currentTime:string;

	// Initialize
	constructor(public nav: NavController,public alert: AlertController, public storage:StorageProvider, public events:Events) { 		
		this.currentTime = new Date().toISOString();
	}

	// Scroll top when view is initialized
	ionViewDidLoad() { this.content.scrollToTop(); }

	// Update hall title
  updateHall(title:any) {	this.hall = title; }

	// Send the order
	sendOrder() {

		console.log("Order page: ready to send an order");

		let page:any = this;

		// Compile new order from variants
		// -------------------------------
		let order:any = [];
		for ( let item in page.storage.cart) {

			// Get product components
			let fullID = item;
			let idComponents = fullID.split("-");
			let productID = null;
			let productPosition = null;
			if (idComponents.length > 1) {
				productID = Number(idComponents[0]);
				productPosition = Number(idComponents[1]);
			} else productID = Number(idComponents[0]);

			// Get real product
			let product = page.storage.products[productID];
			console.log("Order page: product: ", fullID, productID, productPosition, product);

			// Create record				
			let cartItem:any = {"id" : productID, "count" : page.storage.cart[item], "title" : product.title, "price" : product.price};

			// Append with a position data
			if (productPosition != null) {
				cartItem["positionID"] = productPosition;
				let positionData = product.positions[productPosition];
				if (positionData.title != null) cartItem["positionTitle"] = positionData.title;
				if (positionData.price != null) cartItem["price"] = positionData.price;
				if (positionData.units != null) cartItem["positionUnits"] = positionData.units;
			}

			console.log("Order page: cartitem: ", cartItem);

			// Add record to order
			order.push(cartItem);
		}

		// Build an order query object
		// ---------------------------
		let data:any = {"name" : this.name, "hall" : this.hall, "phone" : this.phone,
			"text" : this.text, "time:2" : Math.round(new Date().valueOf()/1000), "order:2" : order };

		console.log("Order page: ", order);

		// Submit data
		// ---------------------------
		this.storage.sendOrder(data).then((status) => {

			let st:any = status;
			let alertMessage:string = "Ошибка при отправке заказа. Возможно нет соединения с сетью";

  		// Send message
  		if (status == true) {
  			alertMessage = "Ваш заказ отправлен. Спасибо!";
  		}

  		// Show popup and close
			let alert = page.alert.create({
				title: alertMessage,
				buttons: [{
					text : "Продложить",
					handler: () => {
						alert.dismiss();
						if (st == true) {
					 		page.storage.cart = {};
							page.events.publish("cart:updated");
							page.nav.setPages([{"page" : "MenuPage"}]);
					 	}					
				 		return false;
					}
				}]
			});

			alert.present();
  	});

	}
}
