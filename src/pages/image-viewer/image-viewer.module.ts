import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicPageModule } from 'ionic-angular';
import { ImageViewerPage } from './image-viewer';

@NgModule({
  declarations: [
    ImageViewerPage,
  ],
  imports: [
    IonicPageModule.forChild(ImageViewerPage),
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  entryComponents: [ ImageViewerPage ],  
})
export class ImageViewerPageModule {}
