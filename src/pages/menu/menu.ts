import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';

/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

	public items:any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, storage: StorageProvider) {
  	this.navCtrl = navCtrl;
  	let categories = storage.categories;
  	this.items = Object.keys(categories).map(key=>categories[key]);
		console.log(this.items.length);
	}

  openCategory(id:any) {
    this.navCtrl.push("MenuCategoryPage", {"id" : id });
  }

  // Get items
  getItems(e: any) {  	
		this.navCtrl.push("MenuSearchPage", { event: e });
  }

  ionViewDidLoad() {
  }

}
