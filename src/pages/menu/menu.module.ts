import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicPageModule } from 'ionic-angular';
import { MenuPage } from './menu';
import { ComponentsModule} from '../../components/components.module';

@NgModule({
  declarations: [
    MenuPage,
  ],
  imports: [
  	ComponentsModule,
    IonicPageModule.forChild(MenuPage),
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  entryComponents: [ MenuPage ],  
})
export class MenuPageModule {}
