import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicPageModule } from 'ionic-angular';
import { MenuCategoryPage } from './menu-category';
import { ComponentsModule} from '../../components/components.module';

@NgModule({
  declarations: [
    MenuCategoryPage,
  ],
  imports: [
  	ComponentsModule,
    IonicPageModule.forChild(MenuCategoryPage),
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  entryComponents: [ MenuCategoryPage ],  
})
export class MenuCategoryPageModule {}
