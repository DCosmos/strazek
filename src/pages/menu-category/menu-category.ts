import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';

@IonicPage()
@Component({
  selector: 'page-menu-category',
  templateUrl: 'menu-category.html',
})
export class MenuCategoryPage {

	public id: number;
	public items:any = [];
	public title:string = "Без названия"
	public sections:string[] = [];

	// Init
  // ------------------------
  constructor(public navCtrl: NavController, public navParams: NavParams, storage: StorageProvider) {
 	 	this.id = this.navParams.get("id");

 	 	// Init category data
 	 	this.title = storage.categories[this.id].title;

 	 	// Init items
 	 	this.items = [];

 	 	// List of categories
 	 	this.sections = [];

 	 	for (let productID in storage.products) {
	    let product = storage.products[productID];
	    if (product.parent == this.id) {

	    	// Add an item to list
	    	this.items.push(product);

	    	// Add a section to list
	    	if (product.section != null) {
	    		if (this.sections.indexOf(product.section) == -1) {
	    			this.sections.push(product.section);
	    		}
	    	}
	    }
		}

  }

  // Open product
  // ------------------------
  openProduct(id:any) {
    this.navCtrl.push("MenuItemPage", {"id" : id });
  }

	noSection(itemList: any[]): any[] {
    let result: any[] = [];
		for (let itemID in this.items) {
			if (this.items[itemID].section == null) result.push(this.items[itemID]);
		}
    return result;
  }

	inSection(itemList: any[], section: string): any[] {
    let result: any[] = [];
		for (let itemID in this.items) {
			if (this.items[itemID].section != null && this.items[itemID].section == section) result.push(this.items[itemID]);
		}
    return result;
  }

}
