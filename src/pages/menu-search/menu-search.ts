import { Component, ViewChild } from '@angular/core';
import { Content, IonicPage, NavController, NavParams } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';
/**
 * Generated class for the MenuSearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu-search',
  templateUrl: 'menu-search.html',
})
export class MenuSearchPage {

	@ViewChild(Content) content: Content;


	public searchText: string;
	public items:any = [];
	public storage: StorageProvider;
	public nav: NavController;

	// Init
  constructor(public navCtrl: NavController, public navParams: NavParams, storageProvider: StorageProvider) {
  	this.storage = storageProvider;
  	this.nav = navCtrl;
  	let searchText = navParams.data.event.data;
  	this.update();
  	console.log(navParams.data.event);
  }

  // Update the list
  update() {

  	let page = this;

  	this.items = [];
  	for (let productID in this.storage.products) {
  		let product = this.storage.products[productID];
  		if (product.title != null && this.searchText != null && product.title.toLowerCase().search(this.searchText.toLowerCase()) != -1 ) {
	  		this.items.push(product);
	  	}
  	}

		if (this.content != null) 	this.content.resize();

  }

  getItems(e: any) {  	
  	if (this.searchText == "") {
  		this.nav.pop();
  		return;
  	}
  	this.update();
  }

  openProduct(id:any) {
    this.navCtrl.push("MenuItemPage", {"id" : id });
  }

}
