import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicPageModule } from 'ionic-angular';
import { MenuSearchPage } from './menu-search';

@NgModule({
  declarations: [
    MenuSearchPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuSearchPage),
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  entryComponents: [ MenuSearchPage ],
})
export class MenuSearchPageModule {}
