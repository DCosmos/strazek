import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';

import { ComponentsModule } from '../../components/components.module';

/**
 * Generated class for the HallsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-halls',
  templateUrl: 'halls.html',
})
export class HallsPage {

	public halls:any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, storage: StorageProvider) {
  	this.halls = Object.keys(storage.halls).map(key=>storage.halls[key]);
  }
}
