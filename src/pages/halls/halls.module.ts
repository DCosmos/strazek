import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicPageModule } from 'ionic-angular';
import { HallsPage } from './halls';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    HallsPage,
  ],
  imports: [
  	ComponentsModule,
    IonicPageModule.forChild(HallsPage),
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  entryComponents: [ HallsPage ],  
})
export class HallsPageModule {}
