import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { CallNumber } from '@ionic-native/call-number';

@IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  constructor(public navCtrl: NavController, private iab: InAppBrowser,private callNumber: CallNumber) {  }

  // Call phone
  callPhone(phone: string) {
    this.callNumber.callNumber(phone, true);
  }

  openLink(link: string) {
    let browser:any = this.iab.create(link,'_blank','location=yes');
    browser.show();
}

}
