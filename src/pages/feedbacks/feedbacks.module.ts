import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicPageModule } from 'ionic-angular';
import { FeedbacksPage } from './feedbacks';

@NgModule({
  declarations: [
    FeedbacksPage,
  ],
  imports: [
    IonicPageModule.forChild(FeedbacksPage),
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  entryComponents: [ FeedbacksPage ],  
})
export class FeedbacksPageModule {

}
