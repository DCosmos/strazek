import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';

/**
 * Generated class for the FeedbacksPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feedbacks',
  templateUrl: 'feedbacks.html',
})

export class FeedbacksPage {

	public navCtrl:NavController;
	public feedbacksList: any;

  constructor(public nav: NavController, public navParams: NavParams, storage: StorageProvider) {
  	this.feedbacksList = Object.keys(storage.feedbacks).map(key=> {
  	  let text = storage.feedbacks[key].text;
  	  if (text !=null && text.length>250) storage.feedbacks[key].text = storage.feedbacks[key].text.substring(0, 250) + '...';
  	  return storage.feedbacks[key];
  	});
  	this.navCtrl = nav;
  }

  addFeedback() {
  	this.navCtrl.push("FeedbackFormPage");
  }

  ionViewDidLoad() {
  }

}
