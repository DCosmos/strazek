import { Component } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams, Platform,Events  } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';
import { ElementRef, ViewChild } from '@angular/core';

@IonicPage()
@Component({ selector: 'page-loading', templateUrl: 'loading.html'})
export class LoadingPage {
	@ViewChild('cover') coverImg: ElementRef;

	public loadingPercent:number = 0;
	public status: string = "Загрузка...";

	// Create
	// ------
  constructor(public storage: StorageProvider, public alert: AlertController, public nav: NavController, public navParams: NavParams, public plt: Platform, private events:Events) {
		this.plt.ready (). then ((readySource) => {
	  	this.storage.update();
    });
  }

  // Close the application with a notice
  // -----------------------------------
  closeApp() {
  }

  // Connect and check for updates
  // -----------------------------
  ionViewDidEnter() {
  	let loadingPage: any = this;

  	// Normal events
  	this.events.subscribe("update:start", () => { loadingPage.status = "Проверка обновлений...";  });
  	this.events.subscribe("update:download", () => { loadingPage.status = "Скачивание обновлений...";  });
  	this.events.subscribe("update:init", () => { loadingPage.status = "Инициализация данных..."; });
  	this.events.subscribe("update:unpack", () => { loadingPage.status = "Распаковка...";  });

  	this.events.subscribe("update:error", () => { loadingPage.error("Ошибка обновления"); });  	
    this.events.subscribe("update:connect-error", () => { loadingPage.error("Ошибка соединения"); });
  	this.events.subscribe("update:network-error", () => { loadingPage.error("Нет соединения с сервером обновления. Проверьте сетевое подключение и попробуйте запустить приложение снова."); });  	
  }

  // Process any loading errors
  // --------------------------------
  error(text : string) {
  	let loadingPage:any = this;
  	this.status = text;
 		// Show popup and close
		let alert = this.alert.create({
			title: text,
			buttons: [{
				text : "Завершить работу",
				handler: () => {
					alert.dismiss();
				 	loadingPage.plt.exitApp();
				 	return false;
				}
			}]
		});
	 	alert.present();
	}
}
