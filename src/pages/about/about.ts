import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})

export class AboutPage {
  nav: NavController;

  constructor(public navCtrl: NavController) {
  	this.nav = navCtrl;
  }

  gotoURL(url:string) {
  }

  openPage(page:any) {
		this.nav.push(page);
  }

}
