import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlertController, Events } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';

/**
 * Generated class for the OrderTablePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-order-table',
  templateUrl: 'order-table.html',
})
export class OrderTablePage {

 	public name:string;
 	public phone:string;
 	public text:string;
 	public hall:string;
 	public time:string;

 	public currentTime:string;

  constructor(public nav: NavController,public alert: AlertController, public storage:StorageProvider, public events:Events) { 
  	this.time = new Date().toISOString();
  	this.currentTime = new Date().toISOString();
  }

  updateHall(title:any) {
  	this.hall = title;
  }

  // Send the order
  sendOrder() {

	let page:any = this;
	let data:any = {"name" : this.name, "phone" : this.phone, "text" : this.text, "time" : this.time, "hall" : this.hall };

		// Submit data
  	this.storage.sendTableOrder(data).then((status) => {

			let st:any = status;
  		let alertMessage:string = "Ошибка при отправке заказа. Возможно нет соединения с сетью";

  		// Send message
  		if (status == true) {
  			alertMessage = "Ваш зкаказ отправлен. Спасибо!";
  		}

  		// Show popup and close
		let alert = page.alert.create({
			title: alertMessage,
			buttons: [{
				text : "Продложить",
				handler: () => {
					alert.dismiss();
					if (st == true) {
						page.nav.setRoot("MenuPage");
				 	}					
				 	return false;
				}
			}]
		});

		alert.present();
  	});

 
  }
}
