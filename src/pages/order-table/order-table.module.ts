import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicPageModule } from 'ionic-angular';
import { OrderTablePage } from './order-table';
import { ComponentsModule} from '../../components/components.module';

@NgModule({
  declarations: [
    OrderTablePage,
  ],
  imports: [
  	ComponentsModule,
    IonicPageModule.forChild(OrderTablePage),
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  entryComponents: [ OrderTablePage ]
})
export class OrderTablePageModule {}
