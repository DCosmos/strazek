import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicPageModule } from 'ionic-angular';
import { MenuItemPage } from './menu-item';
import { ComponentsModule} from '../../components/components.module';

@NgModule({
  declarations: [
    MenuItemPage,
  ],
  imports: [
  	ComponentsModule,
    IonicPageModule.forChild(MenuItemPage),
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  entryComponents: [ MenuItemPage ],
})
export class MenuItemPageModule {}
