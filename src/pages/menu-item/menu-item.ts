import { Component, ViewChild } from '@angular/core';
import { App, Events, Content } from 'ionic-angular';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';
import { File } from '@ionic-native/file';


/**
 * Generated class for the MenuItemPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu-item',
  templateUrl: 'menu-item.html',
})
export class MenuItemPage {


  @ViewChild(Content) content: Content;

	public id: number;
	public title:string;
  public units:string;
	public image:string = "./assets/noimg.jpg";
	public price:number = 0;
	public weight:number;
	public calories:number = 0;
	public positions:any;
	public description:string;

	// Open product
  constructor(private app: App, public navParams: NavParams, public storage: StorageProvider, private file:File) {

  	this.app = app;
  	this.app.setTitle(this.title);

  	// Set id
 	 	this.id = this.navParams.get("id");

 	 	// Init item data
 	 	let product = storage.products[this.id];
    this.units = product.units;
 	 	this.title = product.title;
		this.price = product.price;
		this.weight = product.weight;
    this.description = product.description;
    this.calories = product.calories;

    // Add positions
    if (product.positions != null) {
	    this.positions = product.positions;
	  }
  	// Image

  	let menuItem:any = this;

  	// Image
    if (product.image != null) {
      this.file.readAsDataURL(menuItem.file.dataDirectory + "/assets", product.image).then(dataurl => {
        menuItem.image = dataurl;
      });
    }


  }

}
