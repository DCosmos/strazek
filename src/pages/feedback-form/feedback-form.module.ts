import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicPageModule } from 'ionic-angular';
import { FeedbackFormPage } from './feedback-form';

@NgModule({
  declarations: [
    FeedbackFormPage,
  ],
  imports: [
    IonicPageModule.forChild(FeedbackFormPage),
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  entryComponents: [ FeedbackFormPage ],  
})
export class FeedbackFormPageModule {}
