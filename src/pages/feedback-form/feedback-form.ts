import { Component } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';

/**
 * Generated class for the FeedbackFormPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feedback-form',
  templateUrl: 'feedback-form.html',
})
export class FeedbackFormPage {

	public name:string = "";
	public text:string = "";

  constructor(public nav: NavController, public navParams: NavParams, public alert: AlertController, private storage: StorageProvider) { }

  // On change
  change() {   

  	let feedbackForm:any = this;

  	this.storage.sendFeedback({"name" : this.name, "text" : this.text}).then((status) => {

  		let alertMessage:string = "Ошибка при отправке отзыва. Возможно нет соединения с сетью";
  		let feedbackForm = this;

  		// Send message
  		if (status == true) {
  			alertMessage = "Ваш отзыв отправлен. Спасибо!";
  			feedbackForm.name = "";
  			feedbackForm.text = "";
  		}

  		// Show popup and close
			let alert = feedbackForm.alert.create({
				title: alertMessage,
				buttons: [{
					text : "Продложить",
					handler: () => {
						let navTransition = alert.dismiss();
					 	if (status == true) {
					 		feedbackForm.nav.pop();
					 	}
					 	return false;
					}
				}]
			});

		 	alert.present();

  	});
  }

}
