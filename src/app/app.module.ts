// Main modules
import { NgModule, ErrorHandler, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';

// System
import { InAppBrowser } from "@ionic-native/in-app-browser";
import { CallNumber } from '@ionic-native/call-number';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { HttpModule } from '@angular/http'
import { HttpClientModule } from '@angular/common/http';
import { Zip } from '@ionic-native/zip';
import { IonicStorageModule } from '@ionic/storage';

// Application
import { MyApp } from './app.component';

// Pages
import { AboutPageModule } from '../pages/about/about.module';
import { ContactPageModule } from '../pages/contact/contact.module';
import { OrderPageModule } from '../pages/order/order.module';
import { HallsPageModule } from '../pages/halls/halls.module';
import { OrderTablePageModule } from '../pages/order-table/order-table.module';
import { CartPageModule } from '../pages/cart/cart.module';
import { NewsPageModule } from '../pages/news/news.module';
import { ImageViewerPageModule } from '../pages/image-viewer/image-viewer.module';
import { FeedbacksPageModule } from '../pages/feedbacks/feedbacks.module';
import { FeedbackFormPageModule } from '../pages/feedback-form/feedback-form.module';
import { MenuPageModule } from '../pages/menu/menu.module';
import { MenuSearchPageModule } from '../pages/menu-search/menu-search.module';
import { MenuCategoryPageModule } from '../pages/menu-category/menu-category.module';
import { MenuItemPageModule } from '../pages/menu-item/menu-item.module';
import { LoadingPageModule } from '../pages/loading/loading.module';

// Other
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { ComponentsModule} from '../components/components.module';
//import { MenuCategoryComponent } from '../components/menu';
//import { MenuItemComponent } from '../components/components.module';

// Providers
import { StorageProvider } from '../providers/storage/storage';

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    CommonModule,
	  HttpModule,
	  ComponentsModule,
    IonicModule.forRoot(MyApp,{
      scrollAssist: true,
      autoFocusAssist: true,
      backButtonText: '',
      monthNames: ["Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"],
		  monthShortNames: ["Янв","Фев","Мар","Апр","Май","Июн","Июл","Авг","Сен","Окт","Ноя","Дек"],
		  dayNames: ["понедельник","вторник","среда","четверг","пятница","суббота","воскресенье"]
    }),
	  IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar, SplashScreen, InAppBrowser, CallNumber, FileTransfer, File, Zip, StorageProvider,
    { provide: ErrorHandler, useClass: IonicErrorHandler }    
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
	]
})

// Module functions
export class AppModule {
	updateData() {	}
}
