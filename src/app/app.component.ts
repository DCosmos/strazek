import { Component, ViewChild } from '@angular/core';
import { NavController, MenuController, Events } from 'ionic-angular';

import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { StorageProvider } from '../providers/storage/storage';


@Component({
  templateUrl: 'app.html'
})

export class MyApp {

  // Cart state
  cartIsEmpty: boolean = true;
  notInsideCart: boolean = true;
  cartTotal: number = 0;
  isReady: boolean = false; 

  @ViewChild("content") nav: NavController;

  closeMenu() {
		this.menuCtrl.close();
  }

  openCart() {
		this.nav.setRoot("MenuPage");
   	this.nav.push("CartPage");
  }

  openPage(page:string) {
    console.log('Goto page:', page);
		this.menuCtrl.close();

		if (page == "MenuPage") {
			this.nav.setPages([{ "page" : "MenuPage"}]);
		} else {
			this.nav.setPages([{ "page" : "MenuPage"}, { "page" : page}]);
		}
  }

  // Update cart state
  updateCart() {

	  let app = this;

		// Skip empty cart
		if (Object.keys(app.storage.cart).length === 0) {
			app.cartIsEmpty = true; return;
		}

		// Set the default state
		app.cartIsEmpty = false;

		// Calculate total
		let total:number = 0;

		for(let productID in app.storage.cart) {

			let shortID:any;
			let position: any;

			// Get element short ID
			let idElements = productID.split("-"); 
			if (idElements.length > 1) {
				shortID = Number(idElements[0]);
				position = Number(idElements[1]);
			} else {
				shortID = Number(productID);
			}

			console.log('In cart:', productID, shortID, position);

			// Read product data
			let product = app.storage.products[shortID];
			if (product == null) continue;

			// Add new price
			if (app.storage.cart[productID] != null) {
				let price:number = Number(product.price);
				if (position != null ) price = Number(product.positions[position].price);
				total += price * app.storage.cart[productID];
			}
		}
		// Update application cart
		app.cartTotal = total;
  }

  // Construct update
  // ----------------
  constructor(private platform: Platform, private statusBar: StatusBar, private splashScreen: SplashScreen, public menuCtrl: MenuController, public events: Events, private storage: StorageProvider) {

	  let app = this;
	  console.log("Application is started");

	  // Add cart update event
		this.events.subscribe("cart:updated", () => { app.updateCart(); });

		this.events.subscribe("update:finished", () => {
			app.isReady = true;
			app.nav.setRoot("MenuPage");
		});

		// Update cart status
    this.updateCart();

	  // Init application
    this.platform.ready().then(() => {
      console.log("Platform is ready");
      app.statusBar.styleDefault();
      app.splashScreen.hide();
    });

  }
}
