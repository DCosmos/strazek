import { NgZone, Component, Input, ViewChild, OnInit } from '@angular/core';
import { Events, normalizeURL, Content } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import { File } from '@ionic-native/file';


/**
 * Generated class for the MenuItemComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'menu-item',
  templateUrl: 'menu-item.html'
})
export class MenuItemComponent {

  id:number;
  fullID:string;
 	position:number | null;

  title: string = "не указано";
  description: string;
  price: number = 0;
  weight: number;
  units:string;
  count: number = 0;
  image: string = "";
  product: any;

  positions:any = [];
  _priceFrom: boolean = false;

  // Constructor
  // --------------------------
  constructor(public storage:StorageProvider,  public events:Events, public file:File, private sanitizer: DomSanitizer, private ngZone:NgZone) {
  }

  // Update product
  // --------------------------
  ngOnInit() {
		this.update();
  }

  // Update view
  // ----------------------------
  update() {

  	// Set product info
  	this.title = this.product.title;
    this.units = this.product.units;

    // Set and format a description
    // --------------------------
    if (this.product.description != null) {
	  	this.description = this.product.description.substring(0,60);
  		if (this.product.description.length > 60) this.description += "...";
  	}	

  	// Set price
  	// --------------------------
  	if (this.product.price != null) this.price = this.product.price;

  	// Set positions and correct price
		// ---------------------------
  	if (this.product.positions != null && this.product.positions.length > 1) {
  		this.positions = this.product.positions;

  		// When product's position isn't set
  		if (this.position == null) {
	  		this._priceFrom = true;
  			for (let position of this.positions) {
  				if (Number(position.price) < Number(this.price)) {
  					this.price = position.price;
  					this.units = position.title;
	  			}
  			}
  		}
  		// When product position is set
			else {
				this.price = this.positions[this.position].price;
				this.units = this.positions[this.position].title;
			}
  	}

  }


  // Set item ID
  // --------------------------
  @Input()
  set itemID(id: any) {

    this.fullID = id;
  	
    let menuItem: any = this;

    // Parse id
    // -------------------------
		let idComponents = id.toString().split("-");
		if (idComponents.length > 1) {
			this.id = Number(idComponents[0]);
			this.position = Number(idComponents[1]);
		} else this.id = Number(idComponents[0]);

		// Setup product to display info
		// ------------------------
  	this.product = this.storage.products[this.id];

  	// Load the image
  	// ------------------------
  	if (this.product.image != null) {
      this.file.readAsDataURL(menuItem.file.dataDirectory + "/assets", this.product.image).then(dataurl => {
          menuItem.image = dataurl;
      });
  	}
  }
}
