import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from 'ionic-angular';
import { BrowserModule } from '@angular/platform-browser';

import { MenuItemComponent } from './menu-item/menu-item';
import { MenuCategoryComponent } from './menu-category/menu-category';
import { HallSelectorComponent } from './hall-selector/hall-selector';
import { CartControlsComponent } from './cart-controls/cart-controls';
import { GalleryComponent } from './gallery/gallery';

@NgModule({
	declarations: [
   MenuItemComponent,
   MenuCategoryComponent,
   HallSelectorComponent,
   CartControlsComponent,
   GalleryComponent,
  ],
	imports: [CommonModule, IonicModule],
	schemas: [CUSTOM_ELEMENTS_SCHEMA],
	exports: [
    MenuItemComponent,
    MenuCategoryComponent,
    HallSelectorComponent,
    CartControlsComponent,
    GalleryComponent,
]
})
export class ComponentsModule {}
