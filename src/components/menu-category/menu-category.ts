import { Component, Input, CUSTOM_ELEMENTS_SCHEMA, NgZone  } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MenuItemPage } from '../../pages/menu-item/menu-item';
import { normalizeURL } from 'ionic-angular';
import { File } from '@ionic-native/file';
import { StorageProvider } from '../../providers/storage/storage';

/**
 * Generated class for the MenuCategoryComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'menu-category',
  templateUrl: 'menu-category.html'
})
export class MenuCategoryComponent {

	id: number;
  title: string = "без названия";
  image: string; // = "./assets/imgs/noimg.jpg";
  description: string = "";

  // Consctuctor
  constructor(public nav: NavController, public storage:StorageProvider, private file:File, private ngZone: NgZone) {
  }

  @Input()
  set itemID(id: number) {
    this.id = id;
    let menuCategory: any = this;
  	let category = this.storage.categories[Number(this.id)];

    this.title = category.title;

    // Image
    if (category.image != null) {
      this.file.readAsDataURL(menuCategory.file.dataDirectory + "/assets", category.image).then(dataurl => {
        menuCategory.ngZone.run(() => {
          menuCategory.image = dataurl;
          console.log("menuCategory image", menuCategory.title);
        });                
      });
    }
  }

}
