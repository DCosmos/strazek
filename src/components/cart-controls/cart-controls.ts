import { Component, Input } from '@angular/core';
import { AlertController, Events } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';

@Component({
  selector: "cart-controls",
  templateUrl: "cart-controls.html"
})

export class CartControlsComponent {

	id: string;
	_position: number | null;
	_title: string | null;
	_price: number | null;
	count: number = 0;
  text: string;

  public storage: StorageProvider;
  public alertCtrl: AlertController;
  public eventsCtrl: Events;

 	// Constructor 
  constructor(public storageProvider:StorageProvider, public alert: AlertController, public events:Events) {  	

  	this.storage = storageProvider;
  	this.alertCtrl = alert;
  	this.eventsCtrl = events;
  }

  // Increase value
  inc() {
		this.count = this.count + 1;
		this.storage.cart[this.id] = this.count;
		this.eventsCtrl.publish("cart:updated");
  }

  // Decrease value
  dec() {
		if (this.count > 0) this.count = this.count - 1;
		if (this.count == 0) delete this.storage.cart[this.id];
		else this.storage.cart[this.id] = this.count;
		this.eventsCtrl.publish("cart:updated");
  }

  // Remove item
  remove() {
  	this.count = 0; 

  	let controls = this;
		delete this.storage.cart[this.id];
		let alert = this.alertCtrl.create({
			title: "Товар удален из корзины",
			buttons: [{
				text : "Продложить",
				handler: data => {
					controls.eventsCtrl.publish("cart:updated");
				}								
			}]
		});
	 	alert.present();
  }

  @Input()
  set itemID(id: string) {
  	this.id = id;
    // Add to cart
		if (this.storage.cart == null) return;
		if (this.storage.cart[this.id] != null) this.count = this.storage.cart[this.id];
  }


  @Input()
  set title(title: string | null) {
	    this._title = title;
  }

  @Input()
  set price(price: number | null) {
	    this._price = price;
  }

}
