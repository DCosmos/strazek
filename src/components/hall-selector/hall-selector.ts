import { Component, Input, ViewChild,Output, EventEmitter  } from '@angular/core';
import { StorageProvider } from '../../providers/storage/storage';
import { File } from '@ionic-native/file';
import { Slides } from 'ionic-angular';

/**
 * Generated class for the HallSelectorComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'hall-selector',
  templateUrl: 'hall-selector.html'
})

export class HallSelectorComponent {

	@ViewChild(Slides) slides: Slides;

	public halls: any = [];  
	public hall:any = {};
	public hallTitle:string;

	// Constructor
	constructor(private storage: StorageProvider, private file:File) {
		this.update();
	}

	@Output('onSlide') clickEmitter = new EventEmitter<string>();

	slide() {
		this.hall = this.halls[this.slides.getActiveIndex()];
		if (this.hall != null) this.hallTitle = this.hall.title;		
		this.clickEmitter.emit(this.hallTitle);
		console.log("Slided to", this.hall);
	}

	// Update the list of halls
	update() {

		let hallSelector = this;
		this.halls = [];
		let count = this.storage.halls.length;

		let countNum = 0;
		for (let hallID in this.storage.halls) {
			let hall = this.storage.halls[hallID];

			// Add element to list
			hallSelector.halls.push({
				"image" : null,
				"id": hallID,
				"title": hall.title,
				"description": hall.description,
				"places": hall.seats
			});

			// Load the image
			if (hall.images != null && hall.images[0] != null) {
				let id = countNum;
				hallSelector.file.readAsDataURL(hallSelector.file.dataDirectory + "/assets", hall.images[0]).then(dataurl => {
					console.log("Hall image is loaded for hall", id);
					console.log(id, hallSelector.halls);	
					hallSelector.halls[id].image = dataurl;					
				});
			}

			// Inc
			countNum += 1;
		}
	}
}
