import { Component, Input, ViewChild  } from '@angular/core';
import { Content, Slides, ModalController } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';
import { File } from '@ionic-native/file';

@Component({
  selector: 'gallery',
  templateUrl: 'gallery.html'
})

export class GalleryComponent {

	@ViewChild(Content) content: Content;
	@ViewChild(Slides) slider: Slides;

  id: number;
  _hallID: number;
  images: any = [];
  title: string;
  text: string;

  constructor(private storage: StorageProvider, private modalCtrl: ModalController, private file:File) {
  }

  update() {

  	console.log("Gallery for hall:", this.id);
  	if (this.id == null) return;

  	let hall:any = this.storage.halls[this.id];
  	let gallery: any = this;

  	if (hall.title != null) this.title = hall.title;
  	if (hall.description != null) this.text = hall.description;

  	// Image
    if (hall.images != null) {
      let imagesList = [];
      let count = hall.images.length;

      for (let image of hall.images) {
        this.file.readAsDataURL(gallery.file.dataDirectory + "/assets", image).then(dataurl => {
          imagesList.push(dataurl);
          count = count - 1;
          if (count == 0) {
            gallery.images = imagesList;
          }
        });
      }
    }

    }


  // Zoom an image
  zoom() {
  	let slideIndex = this.slider.getActiveIndex();
		let zoomModal = this.modalCtrl.create("ImageViewerPage", {"images" : this.images});
	  zoomModal.present();
  }

  next() {
  	this.slider.slideNext();
  }

  prev() {
  	this.slider.slidePrev();
  }

  @Input()
  set hallID(id: number) {
    this.id = Number(id);
    this.update();
  }

}
