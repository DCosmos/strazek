<?php

// Errors
error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set("log_errors", 1);

// Add PHPMailer requirements
require_once('libs/PHPMailer/Exception.php');
require_once('libs/PHPMailer/PHPMailer.php');
require_once('libs/PHPMailer/SMTP.php');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// Add ImageResize requirements
require_once('libs/ImageResize/ImageResize.php');
require_once('libs/ImageResize/ImageResizeException.php');
use \Gumlet\ImageResize;


// Process request
// ----------------------------------------

// Skip empty and wrong request
if (empty($_GET['method'])) die("Method is not supported.");

// Include MODx core
require_once('../config.core.php');
if (!include_once(MODX_CORE_PATH . 'model/modx/modx.class.php')) die();

// Create MODx instance
$modx= new modX();
if (!is_object($modx) || !($modx instanceof modX)) { die("MODx is not found."); }
$modx->initialize('web');

// Process methods
$result = [];
header('Content-Type: application/json; charset=utf-8');
switch ($_GET['method']) {
	case "getUpdate": $result = apiGetUpdate(); break;
	case "orderTable": $result = apiOrderTable(); break;
	case "orderCart": $result = apiOrderCart(); break;
	default: $result = false;
}

echo json_encode($result);
die();



// Util. Get TV Value
function getTV($res, $val) {
	global $modx; 
	$tvr = $modx->getObject('modTemplateVarResource', array(
  	'tmplvarid' => $val,
	  'contentid' => $res->get('id')
	));
}

// Util. Get mailer
function getMailer() {

	$mail = new PHPMailer(true);
	$mail->CharSet = 'UTF-8';

 	// Init SMTP
	$mail->isSMTP();
  $mail->Host = 'smtp.gmail.com';
  $mail->SMTPAuth = true;
  $mail->Username = 'vebas.postmaster@gmail.com';
  $mail->Password = '0F133FNkjnf';
  $mail->SMTPSecure = 'tls';
  $mail->Port = 587;    

  // Basic submit properties
  $mail->IsHTML(true);
 	$mail->setFrom('vebas.postmaster@gmail.com', 'Strazek App Mailer');
	$mail->addAddress('magmalt@mail.ru'); 

	return $mail;
}


// Generate unique asset ID for files
function getAssetID($file) {
	return md5($file).'.'.strtolower(pathinfo($file, PATHINFO_EXTENSION));
}


// Submit order to email
function apiOrderTable() {

	// Get mailer	
	$mail = getMailer();

	// Add data
	$mail->Subject = 'Заказ стола (Strazek Application)';
	$mail->Body = '
		<h2>Заказ стола (Strazek Application)</h2>
		<p><strong>Имя пользователя:</strong> '.@ $_POST['name'].'</p>
		<p><strong>Контактный телефон:</strong> '.@ $_POST['phone'].'</p>
		<p><strong>Зал:</strong> '.@ $_POST['hall'].'</p>
		<p><strong>Желаемая дата и время:</strong> '.@ $_POST['time'].'</p>
		<p><strong>Комментарий к заказу:</strong> '.@ $_POST['text'].'</p>
	';

  // Send mail
	$mail->send();
	return ['status' => true, 'code' => "complete"];
}

function apiOrderCart() {
	// Get mailer	
	$mail = getMailer();

	// Load products info
	$productsContent = '';	
	$totalPrice = 0;

	// Add each product to the list
	$products = json_decode($_POST['products']);
	foreach($products as $productID => $productCount) {		

		// Get count
		$count = intval($productCount);
		if (empty($count)) $count = 1;
		$price = 0;

		// Get product
		$productsPages = $modx->getObject('modResource', ['id' => $productID]);

		// Get price
		$tvr = $modx->getObject('modTemplateVarResource', ['tmplvarid' => 9, 'contentid' => productID]);
		if (!empty($tvr)) {
			$price = intval($tvr->get('value'));
		}

		// Add to total
		$totalPrice += $price * $count;

		// Add to content
		$productsContent .= "<p><strong>".$page->get('pagetitle')."</strong> Количество: ". $count." Цена: ".$price;
	}

	// Add data
	$mail->Subject = 'Заказ меню (Strazek Application)';
	$mail->Body = '
		<h2>Заказ меню (Strazek Application)</h2>
		<p><strong>Имя пользователя:</strong> '.@ $_POST['name'].'</p>
		<p><strong>Контактный телефон:</strong> '.@ $_POST['phone'].'</p>
		<p><strong>Зал:</strong> '.@ $_POST['hall'].'</p>
		<p><strong>Желаемая дата и время:</strong> '.@ $_POST['time'].'</p>
		<p><strong>Комментарий к заказу:</strong> '.@ $_POST['text'].'</p>
		<h2>Содержимое заказа:</h2>
		'.$productsContent.'
		<hr/>
		<h2>Общая стоимость заказа</h2>
		<p><strong>'.$totalPrice.'</strong> руб.</p>
	';

  // Send mail
	$mail->send();
	return ['status' => true, 'code' => "complete"];
}

// Compress info into update file
// @MEMO parent, id, pagetitle, content, description, introtext, menuindex, publishedon
// ----------------------------------------
function apiGetUpdate() {

	global $modx;

	// Check update time and skip the update
	// -----------------------------------------------------------
	$updateTime = time();	
	if (file_exists(__DIR__.'/updatetime')) {
		$lastUpdateTime = (int) file_get_contents(__DIR__.'/updatetime');
		if ($updateTime < $lastUpdateTime + 604800) return ['status' => 'false', 'code' => 'notRequired', 'lastUpdateTime' => $lastUpdateTime];
	}

	// Result buffers
	// -----------------------------------------------------------
	$result = [ 'news' => [], 'feedbacks' => [], 'categories' => [], 'products' => [], 'halls' => [] ];
	$resultImages= [];

	// Get news
	// -----------------------------------------------------------
	$newsPages = $modx->getCollection('modResource', array('parent' => 7, 'published' => true));
	foreach($newsPages as $page) {
		$newsItem = [];

		// Skip old records
		$newsItem['publishDate'] = strtotime($page->get('publishedon'));
		if ($newsItem['publishDate'] < (time() - 5000000)) continue; 

		// Fill main fields
		$newsItem['id'] = $page->get('id');
		$newsItem['title'] = $page->get('pagetitle');
		$newsItem['text'] = trim(strip_tags($page->get('content')));
		$newsItem['isEvent'] = false;

		// Get image
		$tvr = $modx->getObject('modTemplateVarResource', ['tmplvarid' => 3, 'contentid' => $newsItem['id']]);
		if (!empty($tvr)) {
			$imageID = getAssetID($tvr->get('value'));
			$newsItem['image'] = $imageID;
			$resultImages[$imageID] = $tvr->get('value');
		}

		// Add to resource list
		$result['news'][$page->get('id')] = $newsItem;
	}

	// Get feedbacks
	// -----------------------------------------------------------
	$feedbacksPages = $modx->getCollection('modResource', array('parent' => 8, 'published' => true));
	foreach($feedbacksPages as $page) {
		$feedbackItem = [];
		$feedbackItem['title'] = $page->get('pagetitle');
		$feedbackItem['publishDate'] = strtotime($page->get('publishedon'));
		$feedbackItem['text'] = trim(strip_tags($page->get('content')));
		$result['feedbacks'][$page->get('id')] = $feedbackItem;
	}

	// Get menu categories
	// -----------------------------------------------------------
	$categoriesPages = $modx->getCollection('modResource', array('parent' => 1366, 'published' => true));
	foreach($categoriesPages as $page)  {
		$categoryItem = [];

		// Fill default fields
		$categoryItem['id'] = $page->get('id');
		$categoryItem['title'] = $page->get('pagetitle');
		$categoryItem['parent'] = $page->get('parent');
		$categoryItem['description'] = trim(strip_tags($page->get('content')));

		// Get cover image
		$tvr = $modx->getObject('modTemplateVarResource', ['tmplvarid' => 3, 'contentid' => $categoryItem['id']]);
		if (!empty($tvr)) {
			$imageID = getAssetID($tvr->get('value'));
			$categoryItem['image'] = $imageID;
			$resultImages[$imageID] = $tvr->get('value');
		}

		// @TODO Count children

		// Add to list
		$result['categories'][$page->get('id')] = $categoryItem;
	}

	// Get menu items
	// -----------------------------------------------------------
	$productsPages = $modx->getCollection('modResource', array('template:IN' => [11, 12, 13, 14, 15, 16, 17, 18, 22, 21, 56, 51], 'published' => true));
	foreach($productsPages as $page)  {

		$productItem = [];

		// Fill default fields
		$productItem['id'] = $page->get('id');
		$productItem['title'] = $page->get('pagetitle');
		$productItem['parent'] = $page->get('parent');
		$productItem['description'] = trim(strip_tags($page->get('content')));

		// Price
		$tvr = $modx->getObject('modTemplateVarResource', ['tmplvarid' => 9, 'contentid' => $productItem['id']]);
		if (!empty($tvr)) { $productItem['price'] = $tvr->get('value'); }

		// Chief
		$tvr = $modx->getObject('modTemplateVarResource', ['tmplvarid' => 10, 'contentid' => $productItem['id']]);
		if (!empty($tvr)) { $productItem['chief'] = intval($tvr->get('value')); }

		// Main image
		$tvr = $modx->getObject('modTemplateVarResource', ['tmplvarid' => 8, 'contentid' => $productItem['id']]);
		if (!empty($tvr)) {
			$imageID = getAssetID($tvr->get('value'));
			$productItem['image'] = $imageID;
			$resultImages[$imageID] = $tvr->get('value');
		}

		// Append the result
		$result['products'][$page->get('id')] = $productItem;
	}


	// Get halls
	// -----------------------------------------------------------
	$hallsPages = $modx->getCollection('modResource', array('parent' => 1820));
	foreach ($hallsPages as $page) {
		$hallItem = [];

		$hallItem['id'] = $page->get('id');
		$hallItem['title'] = $page->get('pagetitle');
		$hallItem['description'] = trim(strip_tags($page->get('introtext')));

		// Get images
		$tvr = $modx->getObject('modTemplateVarResource', ['tmplvarid' => 56, 'contentid' => $hallItem['id']]);
		if (!empty($tvr)) {		

			$hallImes['images'] = [];
			$galleryName = $tvr->get('value');
			$gallery = $modx->getObject('galAlbum',['name' => $galleryName]);
			if (!empty($gallery)) {	

	    	$galleryID = $gallery->get('id');

				// Try to fetch images list from gallery
				$images = $modx->getCollection('galAlbumItem', ['album' => $galleryID]);

				// Append images 
				if (!empty($images)) {
					foreach($images as $image) {
						$img = $modx->getObject('galItem',['id' => $image->get('item')]);

						$imageFileName = 'assets/gallery/'.$img->get('filename');
						$imageID = getAssetID($imageFileName);
						$hallItem['images'][] = $imageID;
						$resultImages[$imageID] = $imageFileName;

					}	// endforeach
				}	// endif				
			}
		}

		// Add to halls list
		$result['halls'][$page->get('id')] = $hallItem;
	}

	// Make update dir
	// ---------------------------------------------------------
	$tempDir = __DIR__.'/temp/'.$updateTime;	mkdir($tempDir, 0777); chmod($tempDir, 0777);
	$assetsDir = $tempDir.'/assets'; mkdir($assetsDir, 0777); chmod($assetsDir, 0777);

	// Copy images and resize
	foreach($resultImages as $imageID => $image) {

	  // Copy image
		$newImageName = $assetsDir.'/'.$imageID;
		copy(dirname(__DIR__).'/'.$image, $newImageName);

		// Resize image
		$imageRes = new ImageResize($newImageName);
		$imageRes->resizeToBestFit(800, 800);
		$imageRes->save($newImageName, null, 80);
	}

	// Put content
	file_put_contents($tempDir.'/info.json', json_encode($result));

	// Zip it
	include_once('libs/zip.php');
	$zip = new Zip();
	$zip->zip_start(__DIR__.'/files/'.$updateTime.'.zip');
	$zip->zip_add([$assetsDir, $tempDir.'/info.json']);
	$zip->zip_end();

	// Remove temp dir
	rrmdir($tempDir);

	// Update timestamp
	file_put_contents(__DIR__.'/updatetime', $updateTime);

}

// Check update time
// ----------------------------------------
function apiCheckUpdate() {
	$latestUpdateTime = (int) file_get_contents("api.dat");
	if ($latestUpdateTime > $_GET['updateTime']) {
		return [
			'status' => true,
			'file' => 'api/'.$latestUpdateTime.'.zip'
		];
	}
	return false;
}


// Util: Recursive deletion of directory
// ----------------------------------------
function rrmdir($dir) { 
   if (is_dir($dir)) { 
     $objects = scandir($dir); 
     foreach ($objects as $object) { 
       if ($object != "." && $object != "..") { 
         if (is_dir($dir."/".$object))
           rrmdir($dir."/".$object);
         else
           unlink($dir."/".$object); 
       } 
     }
     rmdir($dir); 
   } 
 }