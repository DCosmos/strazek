<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET,POST');

// Errors
error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set("log_errors", 1);
set_time_limit(20000);

// Add PHPMailer requirements
require_once('libs/PHPMailer/Exception.php');
require_once('libs/PHPMailer/PHPMailer.php');
require_once('libs/PHPMailer/SMTP.php');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// Add ImageResize requirements
require_once('libs/ImageResize/ImageResize.php');
require_once('libs/ImageResize/ImageResizeException.php');
use \Gumlet\ImageResize;


// Skip empty and wrong request
if (empty($_GET['method'])) die("Method is not supported.");

// Include MODx core
require_once('../config.core.php');
if (!include_once(MODX_CORE_PATH . 'model/modx/modx.class.php')) die();

// Create MODx instance
$modx= new modX();
if (!is_object($modx) || !($modx instanceof modX)) { die("MODx is not found."); }
$modx->initialize('web');

// Process methods
$result = [];
header('Content-Type: application/json; charset=utf-8');
switch ($_GET['method']) {
	case "update/": $result = apiGetUpdate(); break;
	case "send-table-order/": $result = apiSendTableOrder(); break;
	case "send-order/": $result = apiSendOrder(); break;
	case "send-feedback/": $result = apiSendFeedback(); break;
	default: $result = false;
}

echo json_encode($result);
die();



// Util. Get TV Value
function getTV($res, $val) {
	global $modx; 
	$tvr = $modx->getObject('modTemplateVarResource', array(
  	'tmplvarid' => $val,
	  'contentid' => $res->get('id')
	));
}

// Util. Get mailer
function getMailer() {

	$mail = new PHPMailer(true);
	$mail->CharSet = 'UTF-8';
  // Basic submit properties
  $mail->IsHTML(true);
 	$mail->setFrom('noreply@strazek.ru', 'Strazek App Mailer');
	$mail->AddAddress('info@strazek.ru', 'Strazek Manager'); 

	$mail->AddCC('dmitry.volosnihin@gmail.com');
	// $mail->AddCC('povarova_06@mail.ru');
	$mail->AddCC('odi-pavel@mail.ru');
	$mail->AddCC('vitos1983.83@mail.ru');	
	$mail->AddCC('kontrolzakazov@gmail.com');

	return $mail;
}


// Generate unique asset ID for files
function getAssetID($file) {
	return md5($file).'.'.strtolower(pathinfo($file, PATHINFO_EXTENSION));
}


// Submit order to email
function apiSendTableOrder() {

	global $modx;

	// Get mailer	

	$mail = getMailer();


	// Skip strange mails
	if (empty($body['name']) && empty($body['phone'])) {
		return ['status' => true, 'code' => "complete"];
	}

	// Get variables
	$body = json_decode(file_get_contents('php://input'),true);
	$time = @ $body['time'];
	$hall = @ $body['hall'];
	$name = @ $body['name'];
	$text = @ $body['text'];
	$phone = @ $body['phone'];

	// Add data
	$mail->Subject = 'Заказ стола (Strazek Application)';
	$mail->Body = "
		<h2>Заказ стола (Strazek Application)</h2>
		<p><strong>Имя пользователя:</strong> $name</p>
		<p><strong>Контактный телефон:</strong> $phone</p>
		<p><strong>Зал:</strong> $hall</p>
		<p><strong>Желаемая дата и время:</strong> $time</p>
		<p><strong>Комментарий к заказу:</strong> $text</p>
	";

  // Send mail
	$mail->send();
	return ['status' => true, 'code' => "complete"];
}

// Submit order to email
function apiSendOrder() {

	// Get mailer	
	$mail = getMailer();
	
	global $modx;

	// Skip strange mails
	if (empty($body['name']) && empty($body['phone'])) {
		return ['status' => true, 'code' => "complete"];
	}

	// Get variables
	$body = json_decode(file_get_contents('php://input'),true);
	$time = @ $body['time'];
	$order = @ $body['order'];
	$name = @ $body['name'];
	$text = @ $body['text'];
	$phone = @ $body['phone'];

	$total = 0;
	$content = "";

	// Add content to order
	foreach($order as $itemID => $itemCount) {
		$page = $modx->getObject('modResource', ['id' => $itemID]);
		if (!empty($page)) {
			$title = $page->get('pagetitle');

			// Get price
			$tvr = $modx->getObject('modTemplateVarResource', ['tmplvarid' => 9, 'contentid' => $itemID]);
			if (!empty($tvr)) {
				$pPrice = $tvr->get('value');
			}
			if (empty($pPrice)) {
			// Second price
				$tvr = $modx->getObject('modTemplateVarResource', ['tmplvarid' => 13, 'contentid' => $itemID]);
				if (!empty($tvr)) $pPrice = $tvr->get('value');
			}
			if (empty($pPrice)) {
			// Second price
				$tvr = $modx->getObject('modTemplateVarResource', ['tmplvarid' => 23, 'contentid' => $itemID]);
				if (!empty($tvr)) $pPrice = $tvr->get('value');
			}
			if (empty($pPrice)) $pPrice = 0;

			$pTotal = (int) $itemCount * $pPrice;
			$total += $pTotal;
			$content .= "<p><strong>$title :</strong> x $itemCount = <strong>$pTotal</strong> руб.</p>";
		}
	}

	$hall = @ $body['hall'];

	// Add data
	$mail->Subject = 'Бронирование блюда (Strazek Application)';
	$mail->Body = "
		<h2>Заказ доставки</h2>
		<p><strong>Имя:</strong> $name</p>
		<p><strong>Контактный телефон:</strong> $phone</p>
		<p><strong>Желаемая дата и время:</strong> $time</p>
		<p><strong>Зал:</strong> $hall</p>
		<p><strong>Комментарий к заказу:</strong> $text</p>
		<hr>
		<h2>Содержимое:</h2>
		$content
		<hr>
		<p><strong>Общая сумма: </strong> $total</p>
	";

  // Send mail
	$mail->send();
	return ['status' => true, 'code' => "complete"];
}

// Submit order to email
function apiSendFeedback() {

	// Get mailer	
	$mail = getMailer();

	$body = json_decode(file_get_contents('php://input'),true);


	// Skip strange mails
	if (empty($body['name']) && empty($body['text'])) {
		return ['status' => true, 'code' => "complete"];
	}



	$name = @ $body['name'];
	$text = @ $body['text'];


	// Add data
	$mail->Subject = 'Новый отзыв (Strazek Application)';
	$mail->Body = "
		<h2>Новый отзыв через приложение (Strazek Application)</h2>
		<p><strong>Имя пользователя:</strong>$name</p>
		<p><strong>Текст отзыва:</strong>$text</p>";

  // Send mail
	$mail->send();
	return ['status' => true, 'code' => "complete"];
}


function apiOrderCart() {
	// Get mailer	
	$mail = getMailer();

	// Load products info
	$productsContent = '';	
	$totalPrice = 0;

	// Skip strange mails
	if (empty($_POST['name']) && empty($_POST['phone'])) {
		return ['status' => true, 'code' => "complete"];
	}

	// Add each product to the list
	$products = json_decode($_POST['products']);
	foreach($products as $productID => $productCount) {		

		// Get count
		$count = intval($productCount);
		if (empty($count)) $count = 1;
		$price = 0;

		// Get product
		$productsPages = $modx->getObject('modResource', ['id' => $productID]);

		// Get price
		$tvr = $modx->getObject('modTemplateVarResource', ['tmplvarid' => 9, 'contentid' => productID]);
		if (!empty($tvr)) {
			$price = intval($tvr->get('value'));
		}

		// Add to total
		$totalPrice += $price * $count;

		// Add to content
		$productsContent .= "<p><strong>".$page->get('pagetitle')."</strong> Количество: ". $count." Цена: ".$price;
	}

	// Add data
	$mail->Subject = 'Заказ меню (Strazek Application)';
	$mail->Body = '
		<h2>Заказ меню (Strazek Application)</h2>
		<p><strong>Имя пользователя:</strong> '.@ $_POST['name'].'</p>
		<p><strong>Контактный телефон:</strong> '.@ $_POST['phone'].'</p>
		<p><strong>Зал:</strong> '.@ $_POST['hall'].'</p>
		<p><strong>Желаемая дата и время:</strong> '.@ $_POST['time'].'</p>
		<p><strong>Комментарий к заказу:</strong> '.@ $_POST['text'].'</p>
		<h2>Содержимое заказа:</h2>
		'.$productsContent.'
		<hr/>
		<h2>Общая стоимость заказа</h2>
		<p><strong>'.$totalPrice.'</strong> руб.</p>
	';

  // Send mail
	$mail->send();
	return ['status' => true, 'code' => "complete"];
}


// Get price positions for product
		// firstPrice - 23, firstName - 20, firstDesc -  22, firstLongName - 21
		// secondPrice - 13, secondName - 12, secondDesc -  15, secondLongName - 16
		// thirdPrice - 29, thirdName - 26, thirdDesc -  28, thirdLongName - 27
		// fourthPrice - 34, fourthName - 31, fourthDesc -  33, fourthLongName - 32

function getProductPositions($productID) {

	global $modx;

	$result = [];

	// Get product
	$product = $modx->getObject('modResource', $productID);
	if (empty($product)) return;

	// If product is not multielement, get values
	if ($product->get('template') != '11') {

		$item = [];

		$item['title'] = $product->get('pagetitle');

		$tvr = $modx->getObject('modTemplateVarResource', ['tmplvarid' => 9, 'contentid' => $productID]);
		if (!empty($tvr)) $item['price'] = $tvr->get('value');

		$units = $product->get('longtitle');
		if (!empty($units)) $item['units'] = $units;

		$item['description'] = trim(strip_tags($product->get('content')));
		$result[] = $item;
	}

	// Multielements
	else {

		$tvSets = [
			['price' => 23, 'name' => 20, 'units' => 21],
			['price' => 13, 'name' => 12, 'units' => 26],
			['price' => 29, 'name' => 26, 'units' => 27],
			['price' => 34, 'name' => 31, 'units' => 32]
		];

		foreach($tvSets as $id => $tvSet) {
			$item = [];

			$tvr = $modx->getObject('modTemplateVarResource', ['tmplvarid' => $tvSet['price'], 'contentid' => $productID]);
			if (!empty($tvr) && !empty($tvr->get('value'))) $item['price'] = $tvr->get('value');

			$tvr = $modx->getObject('modTemplateVarResource', ['tmplvarid' => $tvSet['name'], 'contentid' => $productID]);
			if (!empty($tvr) && !empty($tvr->get('value'))) $item['title'] = $tvr->get('value');

			$tvr = $modx->getObject('modTemplateVarResource', ['tmplvarid' => $tvSet['units'], 'contentid' => $productID]);
			if (!empty($tvr) && !empty($tvr->get('value'))) $item['units'] = $tvr->get('value');

			if (!empty($item)) {
				$item['id'] = $id;
				$result[] = $item;
			}	
		}
	}

	if (!empty($result)) return $result;
}

// Compress info into update file
// @MEMO parent, id, pagetitle, content, description, introtext, menuindex, publishedon
// ----------------------------------------
function apiGetUpdate() {

	global $modx;

	// Check update time and skip the update
	// -----------------------------------------------------------
	$updateTime = time();	
	if (file_exists(__DIR__.'/updatetime')) {
		$lastUpdateTime = (int) file_get_contents(__DIR__.'/updatetime');
		if ($updateTime < $lastUpdateTime + 604800) return ['status' => 'false', 'code' => 'notRequired', 'lastUpdateTime' => $lastUpdateTime];
	}

	// Result buffers
	// -----------------------------------------------------------
	$result = [ 'news' => [], 'feedbacks' => [], 'categories' => [], 'products' => [], 'halls' => [] ];
	$resultImages= [];

	// Get news
	// -----------------------------------------------------------
	$newsPages = $modx->getCollection('modResource', array('parent' => 7, 'published' => true));
	foreach($newsPages as $page) {
		$newsItem = [];

		// Skip old records
		$newsItem['publishDate'] = strtotime($page->get('publishedon'));
		if ($newsItem['publishDate'] < (time() - 5000000)) continue; 

		// Fill main fields
		$newsItem['id'] = $page->get('id');
		$newsItem['title'] = $page->get('pagetitle');
		$newsItem['text'] = trim(strip_tags($page->get('content')));
		$newsItem['isEvent'] = false;

		// Get image
		$tvr = $modx->getObject('modTemplateVarResource', ['tmplvarid' => 3, 'contentid' => $newsItem['id']]);
		if (!empty($tvr)) {
			$imageID = getAssetID($tvr->get('value'));
			$newsItem['image'] = $imageID;
			$resultImages[$imageID] = $tvr->get('value');
		}

		// Add to resource list
		$result['news'][$page->get('id')] = $newsItem;
	}

	// Get feedbacks
	// -----------------------------------------------------------
	$feedbacksPages = $modx->getCollection('modResource', array('parent' => 8, 'published' => true));
	foreach($feedbacksPages as $page) {
		$feedbackItem = [];
		$feedbackItem['title'] = $page->get('pagetitle');
		$feedbackItem['publishDate'] = strtotime($page->get('publishedon'));
		$feedbackItem['text'] = trim(strip_tags($page->get('content')));
		$result['feedbacks'][$page->get('id')] = $feedbackItem;
	}

	// Get menu categories
	// -----------------------------------------------------------
	$categoriesPages = $modx->getCollection('modResource', array('parent' => 1366, 'published' => true));
	$categoriesList = [];
	foreach($categoriesPages as $page)  {
		$categoryItem = [];

		// Fill default fields
		$categoryItem['id'] = $page->get('id');
		$categoriesList[] = $page->get('id');
		$categoryItem['title'] = $page->get('pagetitle');
		$categoryItem['parent'] = $page->get('parent');
		$categoryItem['description'] = trim(strip_tags($page->get('content')));

		// Get cover image
		$tvr = $modx->getObject('modTemplateVarResource', ['tmplvarid' => 58, 'contentid' => $categoryItem['id']]);
		if (!empty($tvr) && $tvr->get('value') == "1") {
			$tvr = $modx->getObject('modTemplateVarResource', ['tmplvarid' => 8, 'contentid' => $categoryItem['id']]);
			if (!empty($tvr)) {
				$imageID = getAssetID($tvr->get('value'));
				$categoryItem['image'] = $imageID;
				$resultImages[$imageID] = $tvr->get('value');
			}
			// Add to list
			$result['categories'][$page->get('id')] = $categoryItem;
		}
	}


	// Get menu items
	// -----------------------------------------------------------
//	$productsPages = $modx->getCollection('modResource', array('template:IN' => [11, 12, 13, 14, 15, 16, 17, 18, 22, 21, 56, 51], 'published' => true));
	$productsPages = $modx->getCollection('modResource', array('parent:IN' => $categoriesList, 'template:IN' => [10, 11, 12, 13, 14, 15, 16, 17, 18, 22, 21, 56, 51], 'published' => true));
	foreach($productsPages as $page)  {

		$productItem = [];

		// Fill default fields
		$productItem['id'] = $page->get('id');
		$productItem['title'] = $page->get('pagetitle');
		$productItem['parent'] = $page->get('parent');
		$productItem['description'] = trim(strip_tags($page->get('content')));

		// Get positions
		$positions = getProductPositions($page->get('id'));
		if (!empty($positions)) $productItem['positions'] = $positions;


		// Get units
		if (!empty($page->get('longtitle'))) $productItem['units'] = $page->get('longtitle'); 


		if ($productItem['parent'] == 1367 || $productItem['parent'] == 1380 ) $productItem['units'] = '0.5 л.';

		// Price
		$tvr = $modx->getObject('modTemplateVarResource', ['tmplvarid' => 9, 'contentid' => $productItem['id']]);
		if (!empty($tvr)) {
			$productItem['price'] = $tvr->get('value');
		}
		if (empty($productItem['price'])) {

			// Second price
			$tvr = $modx->getObject('modTemplateVarResource', ['tmplvarid' => 13, 'contentid' => $productItem['id']]);
			if (!empty($tvr)) {
				$productItem['price'] = $tvr->get('value');
				// Get units
				$tvr = $modx->getObject('modTemplateVarResource', ['tmplvarid' => 12, 'contentid' => $productItem['id']]);
				if (!empty($tvr)) $productItem['units'] = $tvr->get('value');
			}

		}
		if (empty($productItem['price'])) {
			// First price
			$tvr = $modx->getObject('modTemplateVarResource', ['tmplvarid' => 23, 'contentid' => $productItem['id']]);
			if (!empty($tvr)) {
				$productItem['price'] = $tvr->get('value');
				// Get units
				$tvr = $modx->getObject('modTemplateVarResource', ['tmplvarid' => 20, 'contentid' => $productItem['id']]);
				if (!empty($tvr)) $productItem['units'] = $tvr->get('value');
			}
		}



		// Chief
		$tvr = $modx->getObject('modTemplateVarResource', ['tmplvarid' => 10, 'contentid' => $productItem['id']]);
		if (!empty($tvr)) { $productItem['chief'] = intval($tvr->get('value')); }

		// Main image
		$tvr = $modx->getObject('modTemplateVarResource', ['tmplvarid' => 8, 'contentid' => $productItem['id']]);
		if (!empty($tvr)) {
			$imageID = getAssetID($tvr->get('value'));
			$productItem['image'] = $imageID;
			$resultImages[$imageID] = $tvr->get('value');
		} 
		if (empty($productItem['image'])) {
			$tvr = $modx->getObject('modTemplateVarResource', ['tmplvarid' => 40, 'contentid' => $productItem['id']]);
			if (!empty($tvr)) {
				$imageID = getAssetID($tvr->get('value'));
				$productItem['image'] = $imageID;
				$resultImages[$imageID] = $tvr->get('value');
			}
		}			
		if (empty($productItem['image'])) {
			$tvr = $modx->getObject('modTemplateVarResource', ['tmplvarid' => 24, 'contentid' => $productItem['id']]);
			if (!empty($tvr)) {
				$imageID = getAssetID($tvr->get('value'));
				$productItem['image'] = $imageID;
				$resultImages[$imageID] = $tvr->get('value');
			}
		}			

		// Append the result
		$result['products'][$page->get('id')] = $productItem;
	}


	// Get halls
	// -----------------------------------------------------------
	$hallsPages = $modx->getCollection('modResource', array('parent' => 1862, 'id:!=' => 1864));
	$hallsPages = array_merge($hallsPages, $modx->getCollection('modResource', array('parent' => 1864)));

	foreach ($hallsPages as $page) {
		$hallItem = [];

		$hallItem['id'] = $page->get('id');
		$hallItem['title'] = $page->get('pagetitle');
		$hallItem['description'] = trim(strip_tags($page->get('introtext')));

		// Get images
		$tvr = $modx->getObject('modTemplateVarResource', ['tmplvarid' => 56, 'contentid' => $hallItem['id']]);
		if (!empty($tvr)) {		

			$hallItem['images'] = [];
			$galleryName = $tvr->get('value');
			$gallery = $modx->getObject('galAlbum',['name' => $galleryName]);
			if (!empty($gallery)) {	

	    	$galleryID = $gallery->get('id');

				// Try to fetch images list from gallery
				$images = $modx->getCollection('galAlbumItem', ['album' => $galleryID]);

				// Append images 
				if (!empty($images)) {
					foreach($images as $image) {
						$img = $modx->getObject('galItem',['id' => $image->get('item')]);

						$imageFileName = 'assets/gallery/'.$img->get('filename');
						$imageID = getAssetID($imageFileName);
						$hallItem['images'][] = $imageID;
						$resultImages[$imageID] = $imageFileName;

					}	// endforeach
				}	// endif				
			}
		}

		// Add to halls list
		$result['halls'][$page->get('id')] = $hallItem;
	}

//	var_dump($result['halls']);
//	var_dump($resultImages);
//	die();


	// Make update dir
	// ---------------------------------------------------------
	$tempDir = __DIR__.'/temp/'.$updateTime;	mkdir($tempDir, 0777); chmod($tempDir, 0777);
	$assetsDir = $tempDir.'/assets'; mkdir($assetsDir, 0777); chmod($assetsDir, 0777);

	// Copy images and resize
	foreach($resultImages as $imageID => $image) {

	  // Copy image
		$newImageName = $assetsDir.'/'.$imageID;
		copy(dirname(__DIR__).'/'.$image, $newImageName);

		// Resize image
		$imageRes = new ImageResize($newImageName);
		$imageRes->resizeToBestFit(800, 800);
		$imageRes->save($newImageName, null, 80);
	}

	// Put content
	file_put_contents($tempDir.'/info.json', json_encode($result));

	// Zip it
	include_once('libs/zip.php');
	$zip = new Zip();
	$zip->zip_start(__DIR__.'/files/'.$updateTime.'.zip');
	$zip->zip_add([$assetsDir, $tempDir.'/info.json']);
	$zip->zip_end();

	// Remove temp dir
	rrmdir($tempDir);

	// Update timestamp
	file_put_contents(__DIR__.'/updatetime', $updateTime);
	return ['status' => 'false', 'code' => 'required', 'lastUpdateTime' => $updateTime];


}

// Check update time
// ----------------------------------------
function apiCheckUpdate() {
	$latestUpdateTime = (int) file_get_contents("api.dat");
	if ($latestUpdateTime > $_GET['updateTime']) {
		return [
			'status' => true,
			'file' => 'api/'.$latestUpdateTime.'.zip'
		];
	}
	return false;
}


// Util: Recursive deletion of directory
// ----------------------------------------
function rrmdir($dir) { 
   if (is_dir($dir)) { 
     $objects = scandir($dir); 
     foreach ($objects as $object) { 
       if ($object != "." && $object != "..") { 
         if (is_dir($dir."/".$object))
           rrmdir($dir."/".$object);
         else
           unlink($dir."/".$object); 
       } 
     }
     rmdir($dir); 
   } 
 }